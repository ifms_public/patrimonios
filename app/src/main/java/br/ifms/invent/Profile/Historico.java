package br.ifms.invent.Profile;

/**
 * Created by deni.junior on 03/11/2017.
 */
public class Historico {

    private String numero;
    private String descricao;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
