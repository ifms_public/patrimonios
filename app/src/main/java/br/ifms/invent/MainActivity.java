package br.ifms.invent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;

import br.ifms.invent.Database.DBRepository;
import br.ifms.invent.Profile.Servidor;

public class MainActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Verifica se o usuario esta conectado no IFMS-ADMINISTRATIVO
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();

        // Verifica se o servidor ja se autenticou
        DBRepository database = new DBRepository(this);
        Servidor servidor = database.capturaServidor();

        // Se esta autenticado, redireciona pra tela de menu
        // Senao, redireciona pra tela de login

        Intent intent;
        if (servidor == null){
            intent = new Intent(this, LoginActivity.class);
        } else if (!wifiInfo.getSSID().equals("\"IFMS-ADMINISTRATIVO\"")){
            intent = new Intent(this, WifiActivity.class);
        } else {
            intent = new Intent(this, MenuActivity.class);
        }

        startActivity(intent);
        this.finish();

    }

}