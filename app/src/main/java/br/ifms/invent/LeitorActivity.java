package br.ifms.invent;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;

import org.json.JSONObject;

import br.ifms.invent.Database.DBRepository;
import br.ifms.invent.Network.ConexaoSuap;
import br.ifms.invent.Network.SuapCallback;
import br.ifms.invent.Profile.Patrimonio;

/**
 * Created by deni.junior on 20/04/2017.
 */
public class LeitorActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_leitor);

        iniciaLeitor();
    }

    /*
     *
     *  Resultado da leitura do codigo de barras
     *
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String patrimonio;
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            try {
                // Captura resultado da leitura em patrimonio
                patrimonio = data.getStringExtra("SCAN_RESULT");
                abrirPatrimonio(patrimonio);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (resultCode == RESULT_CANCELED) {
            Toast.makeText(this, "Leitura cancelada!", Toast.LENGTH_SHORT).show();
        }
        this.finish();
    }

    /*
     *
     *  Abre o leitor de codigo de barras
     *
     */
    private void iniciaLeitor() {

        IntentIntegrator integrator = new IntentIntegrator(this);

        // Define o tipo de codigo de barras
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);

        // Define o texto que vai aparecer para o usuario
        integrator.setPrompt("Escaneie um patrimônio");

        // Define um beep de identificacao para o leitor
        integrator.setBeepEnabled(false);

        // Define a camera traseira como a principal
        integrator.setCameraId(0);

        // Inicia o leitor
        integrator.initiateScan();
    }

    // Abre o patrimonio digitado
    private void abrirPatrimonio(String patrimonio) throws Exception {

        ConexaoSuap.capturaPatrimonio(LeitorActivity.this, Long.valueOf(patrimonio), new SuapCallback() {
            @Override
            public void onSuccess(JSONObject jsonObject) {

                if (jsonObject.length() == 0) {
                    Toast.makeText(LeitorActivity.this, "Não foi possível encontrar o patrimônio especificado", Toast.LENGTH_SHORT).show();
                    return;
                }

                Patrimonio.getInstance().capturaPatrimonio(jsonObject);

                // Insere no historico
                DBRepository database = new DBRepository(LeitorActivity.this);
                database.inserirPatrimonio(Patrimonio.getInstance());

                // Abrir menu de patrimonio
                Intent intent = new Intent(LeitorActivity.this, MenuActivity.class);
                startActivity(intent);
                LeitorActivity.this.finish();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(LeitorActivity.this, error, Toast.LENGTH_LONG).show();
            }
        });
    }
}
