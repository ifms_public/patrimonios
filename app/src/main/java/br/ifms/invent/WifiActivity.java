package br.ifms.invent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by deni.junior on 01/11/2017.
 */
public class WifiActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi);

        Button btnRecarregar = (Button) findViewById(R.id.btnRecarregar);
        btnRecarregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verificaWifi();
            }
        });
    }

    private void verificaWifi(){

        // Verifica se o usuario esta conectado no IFMS-ADMINISTRATIVO
        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();

        if (!wifiInfo.getSSID().equals("\"IFMS-ADMINISTRATIVO\"")){
            Toast.makeText(WifiActivity.this, "Seu dispositivo não está conectado ao IFMS-ADMINISTRATIVO ", Toast.LENGTH_SHORT).show();
            return;
        }

        // Volta para o MainActivity
        Intent intent = new Intent(WifiActivity.this, MainActivity.class);
        startActivity(intent);
        this.finish();
    }
}
