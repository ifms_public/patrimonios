package br.ifms.invent.Profile;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by deni.junior on 07/06/2017.
 */
public class Patrimonio {

    // Instancia
    private static Patrimonio instance;

    // Variaveis
    private String numero;
    private String numeroSerie;
    private String estadoConservacao;
    private String valor;
    private String cargaContabil;
    private String descricao;
    private String cargaAtual;
    private String vidaUtil;
    private String sala;

    // Singleton
    public static Patrimonio getInstance(){
        if (instance == null){
            instance = new Patrimonio();
        }
        return instance;
    }

    public static boolean checkInstance(){
        return (instance == null) ? false : true;
    }

    // Captura servidor por JSON
    public void capturaPatrimonio(JSONObject json){
        try{
            instance.numero = json.getString("numero");
            instance.descricao = json.getString("descricao").trim().isEmpty() ? "Não registrado" : json.getString("descricao");
            instance.numeroSerie = json.getString("numero_serie").trim().isEmpty() ? "Não registrado" : json.getString("numero_serie");
            instance.sala = json.getString("sala").trim().isEmpty() ? "Não registrado" : json.getString("sala");
            instance.cargaAtual = json.getString("carga_atual").trim().isEmpty() ? "Não registrado" : json.getString("carga_atual");
            instance.estadoConservacao = json.getString("estado_conservacao").trim().isEmpty() ? "Não registrado" : json.getString("estado_conservacao");
            instance.valor = json.getString("valor").trim().isEmpty() ? "Não registrado" : "R$ " +json.getString("valor");
            instance.cargaContabil = json.getString("carga_contabil").trim().isEmpty() ? "Não registrado" : json.getString("carga_contabil");
            instance.vidaUtil = json.getString("vida_util").trim().isEmpty() ? "Não registrado" : json.getString("vida_util");
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    // Zera o patrimonio
    public void zeraPatrimonio(){
        instance = null;
    }

    public String[] toStringArray(){
        return new String[] {
          instance.numero, instance.descricao, instance.numeroSerie, instance.sala,
          instance.cargaAtual, instance.estadoConservacao, instance.valor, instance.cargaContabil,
          instance.vidaUtil, ""
        };
    }

    public String getNumero() {
        return numero;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }

    public String getEstadoConservacao() {
        return estadoConservacao;
    }

    public String getValor() {
        return valor;
    }

    public String getCargaContabil() {
        return cargaContabil;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getCargaAtual() {
        return cargaAtual;
    }

    public String getVidaUtil() {
        return vidaUtil;
    }

    public String getSala() {
        return sala;
    }
}