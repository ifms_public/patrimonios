package br.ifms.invent.Profile;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Classe Singleton para armazenar os dados do servidor
 *
 * Created by deni.junior on 07/06/2017.
 */
public class Servidor {

    // Variaveis
    private String siape;
    private String nome;
    private String cargo;
    private String setor;
    private String campus;
    private String email;

    // Captura servidor por JSON
    public void capturaServidor(JSONObject json){
        try{
            this.siape = json.getString("siape");
            this.nome = json.getString("nome");
            this.cargo = json.getString("cargo");
            this.setor = json.getString("setor");
            this.campus = json.getString("campus");
            this.email = json.getString("email").trim().isEmpty() ? "Não registrado" : json.getString("email");
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public String[] toStringArray(){
        return new String[] {
                this.siape, this.nome, this.cargo, this.setor,
                this.campus, this.email, ""
        };
    }

    // Getters and setters
    public String getSiape() {
        return siape;
    }

    public String getNome() {
        return nome;
    }

    public String getCargo() {
        return cargo;
    }

    public String getSetor() {
        return setor;
    }

    public String getCampus() {
        return campus;
    }

    public String getEmail() {
        return email;
    }

    public void setSiape(String siape) {
        this.siape = siape;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
