package br.ifms.invent;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import br.ifms.invent.Database.DBRepository;
import br.ifms.invent.Network.ConexaoSuap;
import br.ifms.invent.Network.SuapCallback;
import br.ifms.invent.Profile.Servidor;

/**
 * Created by deni.junior on 20/04/2017.
 */
public class LoginActivity extends AppCompatActivity {

    private EditText txtSiape;
    private EditText txtSenha;
    private Button btnEntrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Esconde a ActionBar
        //getSupportActionBar().hide();

        // Views
        txtSiape = (EditText) findViewById(R.id.txtSiape);
        txtSenha = (EditText) findViewById(R.id.txtSenha);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);

        // Realiza login ao clicar no botao de entrar
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    realizaLogin();
                } catch (Exception e){
                    Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });
    }

    // Realiza o login do servidor
    private void realizaLogin() throws Exception {

        String siape = String.valueOf(txtSiape.getText());
        String senha = String.valueOf(txtSenha.getText());

        ConexaoSuap.efetuaLogin(this, siape, senha, new SuapCallback() {
            @Override
            public void onSuccess(JSONObject jsonObject) throws JSONException {

                System.out.println(jsonObject);

                // Verifica se possui algum resultado
                if (!jsonObject.isNull("erro")){
                    Toast.makeText(LoginActivity.this, jsonObject.getString("erro"), Toast.LENGTH_LONG).show();
                    return;
                }

                // Capturar dados do servidor e insere no banco
                Servidor servidor = new Servidor();
                servidor.capturaServidor(jsonObject);

                DBRepository database = new DBRepository(LoginActivity.this);
                database.inserirServidor(servidor);

                // Chama o menu principal
                Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
                startActivity(intent);
                LoginActivity.this.finish();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(LoginActivity.this, error, Toast.LENGTH_LONG).show();
            }
        });
    }
}
