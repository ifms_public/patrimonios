package br.ifms.invent;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import br.ifms.invent.Database.DBRepository;
import br.ifms.invent.Fragments.SobreFragment;
import br.ifms.invent.Fragments.HistoricoFragment;
import br.ifms.invent.Fragments.PatrimonioFragment;
import br.ifms.invent.Fragments.PerfilFragment;
import br.ifms.invent.Fragments.PesquisaFragment;
import br.ifms.invent.Profile.Patrimonio;
import br.ifms.invent.Profile.Servidor;

public class MenuActivity extends AppCompatActivity {

    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        verificaWifi();

        // Captura usuario
        DBRepository database = new DBRepository(this);
        Servidor servidor = database.capturaServidor();

        // Configuracoes de navegacao
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);

        drawerLayout = (DrawerLayout) findViewById(R.id.navigation_drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);

        if (navigationView != null) {
            setupNavigationDrawerContent(navigationView);
        }

        setupNavigationDrawerContent(navigationView);

        if (Patrimonio.checkInstance()) {
            setFragment(1);
        } else {
            setFragment(0);
        }

        View header = navigationView.getHeaderView(0);
        TextView nome = (TextView) header.findViewById(R.id.nome);
        TextView email = (TextView) header.findViewById(R.id.email);

        nome.setText(servidor.getNome());
        email.setText(servidor.getEmail().equals("Não registrado") ? "" : servidor.getEmail());

    }

    /* TODO
     *
     *  Desenvolver volta ao fragment anterior
     * */
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        System.out.println("Teste: " + getSupportFragmentManager().getFragments().size());
        return;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Define o conteudo que ira aparecer no NavigationDrawer
    private void setupNavigationDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.item_nav_patrimonio:
                                menuItem.setChecked(true);
                                setFragment(0);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.item_nav_historico:
                                menuItem.setChecked(true);
                                setFragment(2);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.item_nav_perfil:
                                menuItem.setChecked(true);
                                setFragment(3);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.item_nav_sobre:
                                menuItem.setChecked(true);
                                setFragment(4);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.item_nav_sair:
                                menuItem.setChecked(true);
                                Toast.makeText(MenuActivity.this,"Usuário deslogado com sucesso",Toast.LENGTH_SHORT).show();
                                drawerLayout.closeDrawer(GravityCompat.START);

                                // Remove usuario da sessao
                                DBRepository database = new DBRepository(MenuActivity.this);
                                database.removeServidor();

                                // Volta pra tela de login
                                Intent intent = new Intent(MenuActivity.this, LoginActivity.class);
                                startActivity(intent);
                                return true;
                        }
                        return true;
                    }
                });
    }

    public void setFragment(int position) {
        verificaWifi();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment replaceFragment = null;
        switch (position) {
            case 0:
                replaceFragment = new PesquisaFragment();
                break;
            case 1:
                replaceFragment = new PatrimonioFragment();
                break;
            case 2:
                replaceFragment = new HistoricoFragment();
                break;
            case 3:
                replaceFragment = new PerfilFragment();
                break;
            case 4:
                replaceFragment = new SobreFragment();
                break;
        }
        fragmentTransaction.replace(R.id.fragment, replaceFragment);
        fragmentTransaction.commit();
    }

    private void verificaWifi(){

        // Verifica se o usuario esta conectado no IFMS-ADMINISTRATIVO
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();

        if (!wifiInfo.getSSID().equals("\"IFMS-ADMINISTRATIVO\"")){
            Intent intent = new Intent(this, WifiActivity.class);
            startActivity(intent);
            this.finish();
        }
    }
}

