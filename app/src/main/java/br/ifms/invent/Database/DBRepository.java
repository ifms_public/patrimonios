package br.ifms.invent.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import br.ifms.invent.Profile.Historico;
import br.ifms.invent.Profile.Patrimonio;
import br.ifms.invent.Profile.Servidor;

/**
 * Created by deni on 05/09/15.
 */

public class DBRepository {

    private DBHelper dbHelper;

    public DBRepository(Context context){
        dbHelper = new DBHelper(context);
    }

    public void inserirServidor(Servidor servidor) {

        // Abre base de dados e insere o servidor
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("siape", servidor.getSiape());
        values.put("nome", servidor.getNome());
        values.put("cargo", servidor.getCargo());
        values.put("setor", servidor.getSetor());
        values.put("campus", servidor.getCampus());
        values.put("email", servidor.getEmail());

        // Insere em servidor
        db.insert("servidor", null, values);
        db.close();
    }

    public void removeServidor() {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete("servidor", "1=1", null);
        db.close();
    }

    public void atualizaServidor(Servidor servidor) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("nome", servidor.getNome());
        values.put("cargo", servidor.getCargo());
        values.put("setor", servidor.getSetor());
        values.put("campus", servidor.getCampus());
        values.put("email", servidor.getEmail());

        db.update("servidor", values, "siape = ?", new String[]{servidor.getSiape()});
        db.close();
    }

    public Servidor capturaServidor(){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "SELECT * FROM servidor";

        Servidor servidor = new Servidor();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() == 0){
            return null;
        }

        if (cursor.moveToFirst()) {
            do {
                servidor.setSiape(cursor.getString(cursor.getColumnIndex("siape")));
                servidor.setNome(cursor.getString(cursor.getColumnIndex("nome")));
                servidor.setCargo(cursor.getString(cursor.getColumnIndex("cargo")));
                servidor.setSetor(cursor.getString(cursor.getColumnIndex("setor")));
                servidor.setCampus(cursor.getString(cursor.getColumnIndex("campus")));
                servidor.setEmail(cursor.getString(cursor.getColumnIndex("email")));

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return servidor;
    }

    public void inserirPatrimonio(Patrimonio patrimonio){

        // Abre base de dados e insere o servidor
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("numero", patrimonio.getNumero());
        values.put("descricao", patrimonio.getDescricao());

        // Insere em servidor
        db.insert("historico", null, values);
        db.close();
    }

    public void limparHistorico(){
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete("historico", "1=1", null);
        db.close();
    }

    public TreeMap<Integer, Historico> capturaHistorico(){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "SELECT * FROM historico";

        TreeMap<Integer, Historico> historico = new TreeMap<>();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                Historico patrimonio = new Historico();
                patrimonio.setDescricao(cursor.getString(cursor.getColumnIndex("descricao")));
                patrimonio.setNumero(cursor.getString(cursor.getColumnIndex("numero")));

                historico.put(cursor.getInt(cursor.getColumnIndex("id")), patrimonio);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return historico;
    }
}
