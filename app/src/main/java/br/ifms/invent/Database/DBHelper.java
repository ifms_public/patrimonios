package br.ifms.invent.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    //version number to upgrade database version
    //each time if you Add, Edit table, you need to change the
    //version number.
    private static final int DATABASE_VERSION = 4;

    // Database Name
    private static final String DATABASE_NAME = "patrimonios.db";

    public DBHelper(Context context ) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // Criacao das tabelas a serem utilizadas
        StringBuilder tabela = new StringBuilder();
        tabela.append("CREATE TABLE servidor (");
        tabela.append("siape TEXT PRIMARY KEY,");
        tabela.append("nome TEXT,");
        tabela.append("cargo TEXT,");
        tabela.append("setor TEXT,");
        tabela.append("campus TEXT,");
        tabela.append("email TEXT);");

        db.execSQL(tabela.toString());

        tabela = new StringBuilder();
        tabela.append("CREATE TABLE historico (");
        tabela.append("id INTEGER PRIMARY KEY AUTOINCREMENT,");
        tabela.append("numero INTEGER,");
        tabela.append("descricao TEXT);");

        db.execSQL(tabela.toString());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Drop older table if existed, all data will be gone!!!
        db.execSQL("DROP TABLE IF EXISTS servidor");
        db.execSQL("DROP TABLE IF EXISTS historico");

        // Create tables again
        onCreate(db);

    }
}
