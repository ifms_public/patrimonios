package br.ifms.invent.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import br.ifms.invent.MenuActivity;
import br.ifms.invent.Profile.Patrimonio;
import br.ifms.invent.R;
import br.ifms.invent.Utils.ListAdapter;

/**
 * Created by deni.junior on 26/06/2017.
 */
public class PatrimonioFragment extends Fragment {

    private String[] titulos;
    private String[] conteudos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_patrimonio, container, false);

        Button btnImagens = (Button) view.findViewById(R.id.btnImagens);
        Button btnConcluir = (Button) view.findViewById(R.id.btnConcluir);
        //TextView headerPatrimonio = (TextView) view.findViewById(R.id.header_patrimonio);

        //headerPatrimonio.setText("Patrimônio "+Patrimonio.getInstance().getNumero());

        this.titulos = new String[]{
                "Número", "Descrição", "Número de Série","Sala","Carga Atual",
                "Estado de Conservação","Valor","Carga Contábil", "Vida útil", ""
        };

        ListView listView = (ListView) view.findViewById(R.id.listView);
        ListAdapter adapter = new ListAdapter(getActivity(), R.layout.item_list_adapter, titulos, Patrimonio.getInstance().toStringArray());
        listView.setAdapter(adapter);

        btnImagens.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                abrirWebFragment();
            }
        });
        btnConcluir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String titulo = ((MenuActivity) getActivity()).getSupportActionBar().getTitle().toString();

                if (titulo.equals("Histórico")){
                    abrirHistoricoFragment();
                } else {
                    abrirPesquisaFragment();
                }


            }
        });

        return view;
    }

    private void abrirWebFragment(){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
        WebFragment webFragment = new WebFragment();
        fragmentTransaction.replace(R.id.fragment, webFragment);
        fragmentTransaction.commit();
    }

    private void abrirPesquisaFragment(){

        Patrimonio.getInstance().zeraPatrimonio();

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
        PesquisaFragment pesquisaFragment = new PesquisaFragment();
        fragmentTransaction.replace(R.id.fragment, pesquisaFragment);
        fragmentTransaction.commit();
    }

    private void abrirHistoricoFragment(){

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
        HistoricoFragment historicoFragment = new HistoricoFragment();
        fragmentTransaction.replace(R.id.fragment, historicoFragment);
        fragmentTransaction.commit();
    }
}
