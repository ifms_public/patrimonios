package br.ifms.invent.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import br.ifms.invent.Database.DBRepository;
import br.ifms.invent.MenuActivity;
import br.ifms.invent.Profile.Patrimonio;
import br.ifms.invent.Profile.Servidor;
import br.ifms.invent.R;
import br.ifms.invent.Utils.ListAdapter;

/**
 * Created by deni.junior on 27/06/2017.
 */
public class PerfilFragment extends Fragment {

    private String[] titulos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_perfil, container, false);
        ((MenuActivity) getActivity()).getSupportActionBar().setTitle("Perfil");

        this.titulos = new String[]{"Siape", "Nome", "Cargo","Setor","Campus", "Email", ""};

        DBRepository database = new DBRepository(getActivity().getApplicationContext());
        Servidor servidor = database.capturaServidor();

        ListView listView = (ListView) view.findViewById(R.id.listView);
        ListAdapter adapter = new ListAdapter(getActivity(), R.layout.item_list_adapter, titulos, servidor.toStringArray());
        listView.setAdapter(adapter);

        return view;
    }
}
