package br.ifms.invent.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;

import br.ifms.invent.Profile.Patrimonio;
import br.ifms.invent.R;

/**
 * Created by deni.junior on 26/06/2017.
 */
public class WebFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_web, container, false);

        // Views
        WebView webView = (WebView) view.findViewById(R.id.webview);
        Button btnConcluir = (Button) view.findViewById(R.id.btnConcluir);

        // Cria query de pesquisa de acordo com o nome da descricao
        StringBuilder descricao = new StringBuilder();
        String[] descricaoSplit = Patrimonio.getInstance().getDescricao().split(" ");

        descricao.append(descricaoSplit[0]);

        for (int i = 1; i < 6; i++){
            descricao.append("+");
            descricao.append(descricaoSplit[i]);
        }

        // Carrega a webView
        webView.loadUrl("https://www.google.com.br/search?q="+descricao.toString()+"&tbm=isch");
        webView.getSettings().setJavaScriptEnabled(true);

        btnConcluir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                abrirPatrimonioFragment();
            }
        });

        return view;
    }

    private void abrirPatrimonioFragment(){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
        PatrimonioFragment patrimonioFragment = new PatrimonioFragment();
        fragmentTransaction.replace(R.id.fragment, patrimonioFragment);
        fragmentTransaction.commit();
    }
}
