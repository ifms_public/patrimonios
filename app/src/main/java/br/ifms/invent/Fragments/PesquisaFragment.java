package br.ifms.invent.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import br.ifms.invent.Database.DBRepository;
import br.ifms.invent.LeitorActivity;
import br.ifms.invent.MenuActivity;
import br.ifms.invent.Network.ConexaoSuap;
import br.ifms.invent.Network.SuapCallback;
import br.ifms.invent.Profile.Patrimonio;
import br.ifms.invent.R;
import br.ifms.invent.WifiActivity;

/**
 * Created by deni.junior on 12/06/2017.
 */
public class PesquisaFragment extends Fragment {

    private EditText txtPatrimonio;
    private Button btnEscanear;
    private Button btnPesquisar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_pesquisa, container, false);

        ((MenuActivity) getActivity()).getSupportActionBar().setTitle("Patrimônio");

        txtPatrimonio = (EditText) view.findViewById(R.id.txtPatrimonio);
        btnEscanear = (Button) view.findViewById(R.id.btnEscanear);
        btnPesquisar = (Button) view.findViewById(R.id.btnPesquisar);

        // Listeners
        btnEscanear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verificaWifi();
                abrirLeitor();
            }
        });

        btnPesquisar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                escondeTeclado();
                verificaWifi();

                String patrimonio = txtPatrimonio.getText().toString();
                System.out.println("Long: "+Long.MAX_VALUE);

                // Verifica possiveis erros
                if (patrimonio.equals("")) {
                    Toast.makeText(getActivity().getApplicationContext(), "Patrimônio vazio!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (patrimonio.length() > 18){
                    Toast.makeText(getActivity().getApplicationContext(), "Patrimônio inválido", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!patrimonio.matches("[0-9]+")){
                    Toast.makeText(getActivity().getApplicationContext(), "Entre apenas com números", Toast.LENGTH_SHORT).show();
                    return;
                }

                try {
                    abrirPatrimonio(patrimonio);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return view;
    }

    // Abre o leitor de patrimonio
    private void abrirLeitor() {

        System.out.println("SDK_INT: " + Build.VERSION.SDK_INT);
        System.out.println("M: " + Build.VERSION_CODES.M);

        // Verifica se a versao atual eh maior que a LOLLIPOP
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            // Se a permissao de uso da camera foi negada, pedir permissao
            if (getActivity().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                // Solicita permissao
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 0);
            } else {

                // Se a permissão já foi concedida, chamar o leitor
                startActivity(new Intent(getActivity(), LeitorActivity.class));
            }
        }
    }

    // Captura o resultado da requisicao de permissao
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        // Verifica se o resultado foi positivo ou negativo, para poder iniciar o escaneamento
        if (grantResults[0] == 0) {
            startActivity(new Intent(getActivity(), LeitorActivity.class));
        } else {
            Toast.makeText(getActivity(), "A permissão de acesso a câmera é necessário para o escaneamento",
                    Toast.LENGTH_SHORT).show();
        }
    }

    // Abre o patrimonio digitado
    private void abrirPatrimonio(String patrimonio) throws Exception {

        // Desabilita botoes durante a pesquisa
        btnPesquisar.setEnabled(false);
        btnPesquisar.setBackgroundColor(Color.GRAY);
        btnEscanear.setEnabled(false);
        btnEscanear.setBackgroundColor(Color.GRAY);

        ConexaoSuap.capturaPatrimonio(getActivity(), Long.valueOf(patrimonio), new SuapCallback() {
            @Override
            public void onSuccess(JSONObject jsonObject) {

                // Habilita botoes apos a pesquisa
                btnPesquisar.setEnabled(true);
                btnPesquisar.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
                btnEscanear.setEnabled(true);
                btnEscanear.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));

                if (jsonObject.length() == 0) {
                    Toast.makeText(getActivity(), "Não foi possível encontrar o patrimônio especificado", Toast.LENGTH_SHORT).show();
                    return;
                }

                Patrimonio.getInstance().capturaPatrimonio(jsonObject);

                // Insere no historico
                DBRepository database = new DBRepository(getContext());
                database.inserirPatrimonio(Patrimonio.getInstance());

                abrirPatrimonioFragment();

                txtPatrimonio.setText("");
            }

            @Override
            public void onError(String error) {
                Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void abrirPatrimonioFragment(){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
        PatrimonioFragment patrimonioFragment = new PatrimonioFragment();
        fragmentTransaction.replace(R.id.fragment, patrimonioFragment);
        fragmentTransaction.commit();
    }

    private void escondeTeclado(){
        InputMethodManager inputManager = (InputMethodManager)
                getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void verificaWifi(){

        // Verifica se o usuario esta conectado no IFMS-ADMINISTRATIVO
        WifiManager wifiManager = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();

        if (!wifiInfo.getSSID().equals("\"IFMS-ADMINISTRATIVO\"")){
            Intent intent = new Intent(getContext(), WifiActivity.class);
            startActivity(intent);
            getActivity().finish();
            return;
        }
    }
}
