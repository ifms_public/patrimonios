package br.ifms.invent.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import br.ifms.invent.MenuActivity;
import br.ifms.invent.R;

/**
 * Created by deni.junior on 27/06/2017.
 */
public class SobreFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_sobre, container, false);

        ((MenuActivity) getActivity()).getSupportActionBar().setTitle("Sobre");

        TextView lblSerti = (TextView) view.findViewById(R.id.lblSerti);

        String coracao = new String(Character.toChars(0x2764));
        lblSerti.setText("Feito com "+coracao+" por SERTI-TL");

        return view;
    }
}
