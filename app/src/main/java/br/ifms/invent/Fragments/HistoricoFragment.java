package br.ifms.invent.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.Key;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import br.ifms.invent.Database.DBRepository;
import br.ifms.invent.MenuActivity;
import br.ifms.invent.Network.ConexaoSuap;
import br.ifms.invent.Network.SuapCallback;
import br.ifms.invent.Profile.Historico;
import br.ifms.invent.Profile.Patrimonio;
import br.ifms.invent.Profile.Servidor;
import br.ifms.invent.R;
import br.ifms.invent.Utils.ListAdapter;
import br.ifms.invent.WifiActivity;

/**
 * Created by deni.junior on 27/06/2017.
 */
public class HistoricoFragment extends Fragment {

    private Button btnLimpar;
    private Button btnCompartilhar;
    private ListView listView;
    private TextView txtSemHistorico;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_historico, container, false);
        ((MenuActivity) getActivity()).getSupportActionBar().setTitle("Histórico");

        btnLimpar = (Button) view.findViewById(R.id.btnLimpar);
        btnCompartilhar = (Button) view.findViewById(R.id.btnCompartilhar);
        listView = (ListView) view.findViewById(R.id.listView);
        txtSemHistorico = (TextView) view.findViewById(R.id.txtSemHistorico);

        ArrayList<String> numeros = new ArrayList<>();
        ArrayList<String> descricoes = new ArrayList<>();
        int indice = 0;

        final DBRepository database = new DBRepository(getActivity().getApplicationContext());
        TreeMap<Integer, Historico> historico = database.capturaHistorico();
        Iterator iterator = historico.descendingMap().entrySet().iterator();

        while (iterator.hasNext()){
            Map.Entry<Integer, Historico> registro = (Map.Entry) iterator.next();

            if (numeros.contains(registro.getValue().getNumero())){
                continue;
            }

            numeros.add(registro.getValue().getNumero());
            descricoes.add(registro.getValue().getDescricao());
            indice++;
        }

        if (numeros.size() == 0){
            listView.setVisibility(View.GONE);
            btnLimpar.setVisibility(View.GONE);
            btnCompartilhar.setVisibility(View.GONE);
            txtSemHistorico.setVisibility(View.VISIBLE);
        } else {
            numeros.add("");
            descricoes.add("");
        }

        ListAdapter adapter = new ListAdapter(getActivity(), R.layout.item_list_adapter, numeros.toArray(new String[0]), descricoes.toArray(new String[0]));
        listView.setAdapter(adapter);

        btnLimpar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limparHistorico();
            }
        });

        btnCompartilhar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                compartilharHistorico(database.capturaHistorico());
            }
        });


        // Pesquisa patrimonio clicado
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                verificaWifi();
                TextView titulo = (TextView) view.findViewById(R.id.titulo);
                String numero = titulo.getText().toString();
                apresentaPatrimonio(numero);
            }
        });

        return view;
    }

    private void apresentaPatrimonio(String numero){

        try {
            ConexaoSuap.capturaPatrimonio(getContext(), Long.valueOf(numero), new SuapCallback() {
                @Override
                public void onSuccess(JSONObject jsonObject) throws JSONException {
                    Patrimonio.getInstance().capturaPatrimonio(jsonObject);
                    abrirPatrimonioFragment();
                }

                @Override
                public void onError(String error) {
                    Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    };

    private void abrirPatrimonioFragment(){

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
        PatrimonioFragment patrimonioFragment = new PatrimonioFragment();
        fragmentTransaction.replace(R.id.fragment, patrimonioFragment);
        fragmentTransaction.commit();
    }

    private void limparHistorico(){
        DBRepository database = new DBRepository(getContext());
        database.limparHistorico();
        listView.setVisibility(View.GONE);
        btnLimpar.setVisibility(View.GONE);
        btnCompartilhar.setVisibility(View.GONE);
        txtSemHistorico.setVisibility(View.VISIBLE);
    }

    private void compartilharHistorico(TreeMap<Integer, Historico> historico){

        // Constroi string para envio
        StringBuilder textoEnvio = new StringBuilder();
        textoEnvio.append("Segue abaixo a relação de patrimônios pesquisados:");
        textoEnvio.append("\n\n");

        // Itera os patrimonios do historico
        Iterator iterator = historico.descendingMap().entrySet().iterator();
        int numPatrimonios = 0;

        while (iterator.hasNext()){
            Map.Entry<Integer, Historico> registro = (Map.Entry) iterator.next();

            textoEnvio.append("---");
            textoEnvio.append(registro.getValue().getNumero());
            textoEnvio.append("---\n");
            textoEnvio.append(registro.getValue().getDescricao());
            textoEnvio.append("\n\n");

            numPatrimonios++;
        }

        // Compartilha informacao
        if (numPatrimonios == 0){
            textoEnvio.append("Não foram registrados patrimônios.");
        } else{
            textoEnvio.append("Totalizando ");
            textoEnvio.append(numPatrimonios);
            textoEnvio.append(" patrimônios pesquisados.");
        }

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, textoEnvio.toString());
        intent.setType("text/plain");
        startActivity(Intent.createChooser(intent, "Compartilhar patrimônios"));

    }

    private void verificaWifi(){

        // Verifica se o usuario esta conectado no IFMS-ADMINISTRATIVO
        WifiManager wifiManager = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();

        if (!wifiInfo.getSSID().equals("\"IFMS-ADMINISTRATIVO\"")){
            Intent intent = new Intent(getContext(), WifiActivity.class);
            startActivity(intent);
            getActivity().finish();
            return;
        }
    }
}
