package br.ifms.invent.Utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import br.ifms.invent.R;

/**
 * Created by deni.junior on 11/05/2017.
 */
public class ListAdapter extends ArrayAdapter<String> {

    private Context context;
    private String[] titulos;
    private String[] conteudos;

    public ListAdapter(Context context, int resource, String[] titulos, String[] conteudos) {
        super(context, resource, conteudos);
        this.titulos = titulos;
        this.conteudos = conteudos;
        this.context = context;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewRow = layoutInflater.inflate(R.layout.item_list_adapter, viewGroup, false);
        TextView titulo = (TextView) viewRow.findViewById(R.id.titulo);
        TextView conteudo = (TextView) viewRow.findViewById(R.id.conteudo);
        titulo.setText(titulos[i]);
        conteudo.setText(conteudos[i]);
        return viewRow;
    }
}
