package br.ifms.invent.Network;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;

/**
 * Realiza a requisição ao SUAP
 *
 * Created by deni.junior on 19/04/2017.
 */
class PatrimonioRequest extends Request<JSONObject> {

    //private static final String URL = "https://suap.ifms.edu.br/patrimonio/api/patrimonio/";
    public static final String URL = "http://10.8.2.250/invent/patrimonio.php";

    private Listener<JSONObject> listener;
    private long patrimonio;

    public PatrimonioRequest(long patrimonio, Listener<JSONObject> responseListener, ErrorListener errorListener) {
        super(Method.POST, URL, errorListener);
        this.listener = responseListener;
        this.patrimonio = patrimonio;
    }

    protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
        Map<String, String> params = new HashMap<>();
        params.put("patrimonio_id", String.valueOf(this.patrimonio));
        params.put("token","82062801e9df9a7182f4dfd2e50b96c491f21df6");
        return params;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        //headers.put("Authorization","Token 82062801e9df9a7182f4dfd2e50b96c491f21df6");
        return headers;
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success(new JSONObject(jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(JSONObject response) {
        listener.onResponse(response);
    }
}