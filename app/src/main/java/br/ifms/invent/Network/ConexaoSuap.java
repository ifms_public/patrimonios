package br.ifms.invent.Network;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Realiza conexão com o SUAP
 *
 * Created by deni.junior on 12/04/2017.
 */
public class ConexaoSuap {


    // Captura o patrimonio selecionado
    public static void capturaPatrimonio(Context context, long patrimonio, final SuapCallback callback) throws Exception {
        PatrimonioRequest jsonRequest = new PatrimonioRequest(patrimonio, createRequestSuccessListener(callback), createRequestErrorListener(callback));
        Volley.newRequestQueue(context, getHurlStack()).add(jsonRequest);
    }

    // Efetua
    public static void efetuaLogin(Context context, String siape, String senha, final SuapCallback callback) throws Exception{
        LoginRequest jsonRequest = new LoginRequest(siape, senha, createRequestSuccessListener(callback), createRequestErrorListener(callback));
        Volley.newRequestQueue(context, getHurlStack()).add(jsonRequest);
    }

    public static void efetuaLogin(Context context, int siape, String senha, final SuapCallback callback) throws Exception{
        LoginRequest jsonRequest = new LoginRequest(siape, senha, createRequestSuccessListener(callback), createRequestErrorListener(callback));
        Volley.newRequestQueue(context, getHurlStack()).add(jsonRequest);
    }

    // Cria um listener para resposta bem sucedida
    private static Response.Listener<JSONObject> createRequestSuccessListener(final SuapCallback callback) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    callback.onSuccess(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    // Cria um listener para resposta mal sucedida
    private static Response.ErrorListener createRequestErrorListener(final SuapCallback callback) {
        return new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    callback.onError(error.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    // Cria um HurlStack para o RequestQueue
    private static HurlStack getHurlStack() throws Exception {
        final SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, getTrustManagers(), new SecureRandom());

        return new HurlStack(null, sc.getSocketFactory()) {
            @Override
            protected HttpURLConnection createConnection(URL url) throws IOException {

                // Conexao convencional
                HttpURLConnection httpsURLConnection = (HttpURLConnection) super.createConnection(url);

                return httpsURLConnection;

                // Conexao SSL
                /*HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
                try {
                    httpsURLConnection.setSSLSocketFactory(sc.getSocketFactory());
                    httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;*/
            }
        };
    }

    // Captura um verificador de Hostname
    private static HostnameVerifier getHostnameVerifier() {
        return new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
    }

    // Captura um TrustManager, que irá lidar com todas as validações de SSL
    private static TrustManager[] getTrustManagers() {
        return new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                    }

                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                        return myTrustedAnchors;
                    }
                }
        };
    }
}
