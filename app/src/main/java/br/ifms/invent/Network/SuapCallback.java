package br.ifms.invent.Network;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Interface que captura a resposta da requisição do SUAP
 *
 * Created by deni.junior on 19/04/2017.
 */
public interface SuapCallback {

    void onSuccess(JSONObject jsonObject) throws JSONException;
    void onError(String error);
}
