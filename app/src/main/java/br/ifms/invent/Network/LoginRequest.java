package br.ifms.invent.Network;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by deni.junior on 07/06/2017.
 */
public class LoginRequest extends Request<JSONObject> {

    //private static final String URL = "https://suap.ifms.edu.br/patrimonio/api/patrimonio/";
    public static final String URL = "http://10.8.2.250/invent/login.php";

    private Response.Listener<JSONObject> listener;
    private String siape;
    private String senha;

    public LoginRequest(int siape, String senha, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener) {
        super(Method.POST, URL, errorListener);
        this.listener = responseListener;
        this.siape = String.valueOf(siape);
        this.senha = senha;
    }

    public LoginRequest(String siape, String senha, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener) {
        super(Method.POST, URL, errorListener);
        this.listener = responseListener;
        this.siape = siape;
        this.senha = senha;
    }

    protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
        Map<String, String> params = new HashMap<>();
        params.put("siape", this.siape);
        params.put("senha", this.senha);
        params.put("token","82062801e9df9a7182f4dfd2e50b96c491f21df6");
        return params;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        //headers.put("Authorization","Token 82062801e9df9a7182f4dfd2e50b96c491f21df6");
        return headers;
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success(new JSONObject(jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(JSONObject response) {
        listener.onResponse(response);
    }
}
